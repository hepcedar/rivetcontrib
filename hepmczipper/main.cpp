#include "HepMC/IO_GenEvent.h"
#include "Gzip_Stream.h"
#include <vector>
#include <iostream>

using namespace HepMC;

int main(int argc, char *argv[])
{
  std::cout<<"  Usage: ./hepmczipper outfile.hepmc.gz infile1.hepmc [infile2.hepmc ...]"<<std::endl;
  if (argc<3) {
    return 1;
  }

  ogzstream outstream;
  outstream.open(argv[1]);
  outstream.precision(6);
  HepMC::IO_GenEvent hepmc_out(outstream);
  hepmc_out.precision(6);
  
  for (int argidx=2; argidx<argc; ++argidx) {
    std::string infilename(argv[argidx]);
    std::istream* instream(NULL);
    if (infilename=="-") {
      instream=&std::cin;
    }
    else {
      instream=new std::ifstream(infilename.c_str());
    }
    HepMC::IO_GenEvent hepmc_in(*instream);

    HepMC::GenEvent* evt_in = hepmc_in.read_next_event();
    int n=1;
    while (evt_in) {
      HepMC::GenEvent* evt_out=new HepMC::GenEvent();
      evt_out->set_event_number(evt_in->event_number());
      evt_out->use_units(evt_in->momentum_unit(), evt_in->length_unit());
      if (evt_in->cross_section())
        evt_out->set_cross_section(*evt_in->cross_section());
      if (evt_in->pdf_info())
        evt_out->set_pdf_info(*evt_in->pdf_info());
      evt_out->weights()=evt_in->weights();
      
      HepMC::GenVertex * vertex=new HepMC::GenVertex();
      std::vector<HepMC::GenParticle*> beamparticles;
      for (HepMC::GenEvent::particle_const_iterator p 
             = evt_in->particles_begin(); p != evt_in->particles_end(); ++p ){
        if ((*p)->production_vertex()==NULL) {
          GenParticle* newpart=new GenParticle(**p);
          vertex->add_particle_in(newpart);
          beamparticles.push_back(newpart);
        }
        if ((*p)->end_vertex()==NULL) {
          if (!(*p)->is_undecayed()) {
            std::cout<<" Warning: Undecayed particle with status != 1."<<std::endl;
          }
          GenParticle* newpart=new GenParticle(**p);
          vertex->add_particle_out(newpart);
        }
      }
      evt_out->add_vertex(vertex);
      
      if (beamparticles.size()==2) {
        evt_out->set_beam_particles(beamparticles[0], beamparticles[1]);
      }
      else {
        std::cout<<" Warning: Found "<<beamparticles.size()<<" beam particles."<<std::endl;
      }

      hepmc_out.write_event(evt_out);
      delete evt_out;
      delete evt_in;
      evt_in=hepmc_in.read_next_event();
      if (n%1000==0) std::cout<<"Passed "<<n<<" events."<<std::endl;
      ++n;
    }
  }

  return 0;
}
