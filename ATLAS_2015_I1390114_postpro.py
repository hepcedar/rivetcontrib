#! /usr/bin/env python
# post-processing script for ATLAS_2015_I1390114
import yoda
from math import sqrt

hists = yoda.read( 'final.yoda' )

tags = hists.keys()
hName = '/ATLAS_2015_I1390114/d01-x01-y01'
rName = '/ATLAS_2015_I1390114/d02-x01-y01'

h = hists[hName]
ratio = hists[rName]

f = open('final_mod.yoda', 'w')
for tag in tags:
  if '_' in tag[:2]:  yoda.writeYODA(hists[tag], f)
  elif '_aux' in tag: # construct ratio
    yoda.writeYODA(hists[tag], f)
    hasWeights = h.effNumEntries != h.numEntries
    n = h.bins[3].sumW
    d = hists[tag].bins[0].sumW
    r = n/d if d else 0.0
    dN = h.bins[3].sumW2
    dD = hists[tag].bins[0].sumW2
    e = 100. * sqrt(r * (1 - r) / d) if d else 0.0
    if hasWeights:
      e = 100. * sqrt( ((1 - 2 * r) * dN + r ** 2 * dD) / d ** 2 ) if d else 0.0
    ratio.points[0].y = 100. * r
    ratio.points[0].yErrs = (e, e)
    yoda.writeYODA(h, f)
    yoda.writeYODA(ratio, f)
f.close()

