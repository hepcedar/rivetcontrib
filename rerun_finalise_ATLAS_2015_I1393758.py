
import yoda
from math import sqrt

hname = 'outputFileName'

hists = yoda.read('%s.yoda' % hname)
tags = sorted(hists.keys())

f = open('%s_mod.yoda' % hname, 'w')
for h in tags:
  ident = int(h[-10:-8])
  tag = h[:-10] + '0%i' + h[-8:]
  if ident >= 7:
    pos = tag % (ident - 6)
    print ident, pos
    for i in range(hists[h].numPoints):
      bin = hists[pos].bins[i]
      yval = bin.stdDev if bin.numEntries > 1. else 0.0
      yerr = sqrt(bin.sumW2) / (2. * bin.sumW) if bin.sumW else 0.0

      hists[h].points[i].y = yval
      hists[h].points[i].yErrs = (yerr, yerr)
  yoda.writeYODA(hists[h], f)
f.close()

