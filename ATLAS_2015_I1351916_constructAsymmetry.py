import yoda

file_name = 'output'

hists = yoda.read( '%s.yoda' % file_name )
tags = sorted( hists.keys() )
f = open('%s_final.yoda' % file_name, 'w')

# output yoda file should only contain Scatter2D objects ending in y01
# objects ending in y02 are of type Histo1D and correspond to the positive cosThetaStar contribution
# objects ending in y03 are of type Histo1D and correspond to the negative cosThetaStar contribution
# the corresponding Scatter2D objects ending in y01 are given by (POS - NEG) / (POS + NEG)

for h in tags:
  if 'ATLAS_2015_I1351916' not in h:  continue
  if 'y01' not in h:  continue
  path = h[:-1] + '%i'
  hists[h] = yoda.core.divide(hists[ path % 2 ] - hists[ path % 3 ], hists[ path % 2 ] + hists[ path % 3 ])
  hists[h].setAnnotation('Path', h)
  yoda.writeYODA(hists[h], f)

f.close()
