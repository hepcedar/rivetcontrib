#!/usr/bin/env python



def getInSpireKey(ananame):
    searchstring = ananame.split("_S")[-1].split("_")[0]

    import urllib2
    searchpage = urllib2.urlopen('http://inspire-hep.net/search?p=find+key+%s'%searchstring).read()

    from BeautifulSoup import BeautifulSoup
    soup = BeautifulSoup(searchpage)
    soup.prettify()
    result = ""
    for anchor in soup.findAll('a', href=True):
        if anchor['href'].startswith("http://inspirehep.net/record/"):
            result = anchor['href']
            break

    # if result == "":
        # import sys
        # print ananame, "Gone Wrong"
    return str(result.split('/')[-1])




import os, sys

with open(sys.argv[1]) as f:
    SLIST=[i.strip() for i in f]

ILIST=[]
print "Gathering information from Inspire..."
toolbar_width = len(SLIST)

# setup toolbar
sys.stdout.write("[%s]" % (" " * toolbar_width))
sys.stdout.flush()
sys.stdout.write("\b" * (toolbar_width+1)) # return to start of line, after '['

for i in xrange(toolbar_width):
    ILIST.append(getInSpireKey(SLIST[i]))
    # update the bar
    sys.stdout.write("-")
    sys.stdout.flush()
sys.stdout.write("\n")
# ILIST = [getInSpireKey(s) for s in SLIST]

with open("translation.txt", "w") as f:
    for num , I in enumerate(ILIST):
        f.write("%s\t%s\n"%(SLIST[num], I))

print "Wrote 'translation.txt'"
# from IPython import embed

# embed()
