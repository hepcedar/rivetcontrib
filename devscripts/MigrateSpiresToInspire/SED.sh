#!/bin/bash

old=${1}
new=${2}

old=CDF_2009_S8383952
new=CDF_2009_I856131


#cd src/Analyses/
#sed -i "s|${old}|${new}|g" ${old}.cc 
#sed -i "s|${old}|${new}|g" Makefile.am
#hg mv ${old}.cc ${new}.cc
#cd -

#cd data/anainfo
#sed -i "s|${old}|${new}|g" ${old}.info 
#sed -i "s|${old}|${new}|g" Makefile.am
#hg mv ${old}.info ${new}.info
#cd -

#cd data/plotinfo
#sed -i "s|${old}|${new}|g" ${old}.plot 
#sed -i "s|${old}|${new}|g" Makefile.am
#hg mv ${old}.plot ${new}.plot
#cd -

#cd data/refdata
#sed -i "s|${old}|${new}|g" ${old}.yoda
#sed -i "s|${old}|${new}|g" Makefile.am
#hg mv ${old}.yoda ${new}.yoda
#cd -
