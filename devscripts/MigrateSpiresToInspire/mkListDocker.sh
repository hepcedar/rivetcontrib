#!/bin/bash

IMG=iamholger/mcnetbl1:0.1

#docker pull $IMG 

rivet="docker run -it $IMG rivet"


if [ -f "spires_list.raw" ]
then
    rm -f spires_list.raw
fi
if [ -f "inspire_list.raw" ]
then
    rm -f inspire_list.raw
fi


RIVETVERSION=`$rivet --version |awk '{print $2}' | sed  "s|v||g"`



echo "Getting analyses present in $IMG Rivet version ${RIVETVERSION}"

$rivet --list-analyses | grep -v EXAMPLE_ |grep -v MC_ |grep -v _I | grep _S  | cut -d " " -f 1 > spires_list.raw
$rivet --list-analyses | grep -v EXAMPLE_ |grep -v MC_ |grep -v _S | grep _I  | cut -d " " -f 1 > inspire_list.raw

python bs_inspire.py spires_list.raw 
./mkHepDataJSON.py translation.txt inspire_list.raw
