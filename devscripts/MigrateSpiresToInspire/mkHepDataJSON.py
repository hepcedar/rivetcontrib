#!/usr/bin/env python

import os, sys




# Read in the SPIRESANA  INSPIREKEY translation file
with open(sys.argv[1]) as f:
    T=[i.split() for i in f]

# Read in the INSPIRE analyses
with open(sys.argv[2]) as f:
    for line in f:
        l = line.strip()
        T.append([l, l.split("_I")[-1].split("_")[0]])

# Produce dict with inspireid as keys and list of corresponding analyses as values
J={}

for t in T:
    try:
        key = t[1]
        ana = t[0]
        if not key in J.keys():
            J[key]=[]
        J[key].append(ana)
    except Exception, e:
        print "Exception:", e
        print "when processing", t

import json
with open('list_of_analyses.json', 'w') as f:
    json.dump(J, f)

print "Remember to make sure that the previous analyses.json is backed up on hepforge before"
print "copying the new one."
print "scp list_of_analyses.json login.hepforge.org:/hepforge/home/rivet/public_html/analyses.json"
