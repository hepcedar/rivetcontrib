#!/usr/bin/env python

import os, sys

def mkSed(ana, iid, path=None):
    cmd = 'sed -i "/SpiresID/a InspireID: %s" '%iid
    if path is not None:
        cmd += os.path.join(path,"%s.info"%ana)
    else:
        cmd += "%s.info"%ana

    return cmd


with open(sys.argv[1]) as f:
    T=[i.split() for i in f]


for t in T:
    os.system(mkSed(t[0], t[1], sys.argv[2]))


