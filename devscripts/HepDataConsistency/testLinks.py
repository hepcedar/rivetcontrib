#!/usr/bin/env python

import rivet
all_analyses = [a for a in rivet.AnalysisLoader.analysisNames() if not a.startswith("MC") and not a.startswith("EXAMPLE") and not a.startswith("PDG")]

import sys
if len(sys.argv)>1:
    temp = []
    for a in all_analyses:
        for k in sys.argv[1:]:
            if k in a:
                temp.append(a)
    all_analyses = list(set(temp))


def hasInspire(ananame):
    import rivet
    return len(rivet.AnalysisLoader.getAnalysis(ananame).inspireId())>0

def isDeadLink(url):
    import requests
    request = requests.get(url)

    if request.status_code == 200:
        return False
    else:
        print('Web site does not exist')
        print url
        return True

print "Checking %i analyses"%len(all_analyses)
insp_url = "http://inspire-hep.net/record/"
hepd_url = "http://hepdata.cedar.ac.uk/view/ins"
for num, a in enumerate(all_analyses):
    if not hasInspire(a):
        print a, "Does not have an InspireID"
    else:
        anaid = rivet.AnalysisLoader.getAnalysis(a).inspireId()
        if isDeadLink(insp_url+anaid):
            print a, "Inspire Page does not exist at", insp_url+anaid
        if isDeadLink(hepd_url+anaid):
            print a, "HepData Page does not exist", hepd_url+anaid
    if (num+1)%10 ==0:
        print "Processed %i/%i analyses"%(num+1, len(all_analyses))

