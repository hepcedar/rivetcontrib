#!/usr/bin/env python

import rivet
all_analyses = sorted([a for a in rivet.AnalysisLoader.analysisNames() if not a.startswith("MC") and not a.startswith("EXAMPLE") and not a.startswith("PDG")])

# t=""
# for i in sorted(list(set([x.split("_")[0] for x in all_analyses]))):
    # t+= '"%s" '%i
# print t

import sys
if len(sys.argv)>1:
    temp = []
    for a in all_analyses:
        for k in sys.argv[1:]:
            if k in a:
                temp.append(a)
    all_analyses = list(set(temp))


def hasInspire(ananame):
    import rivet
    return len(rivet.AnalysisLoader.getAnalysis(ananame).inspireId())>0

def isDeadLink(url):
    import requests
    request = requests.get(url)

    if request.status_code == 200:
        return False
    else:
        print('Web site does not exist')
        print url
        return True

# Stolen from cmphistos
def getRivetRefData(ana):
    "Find all Rivet reference data files"
    import os, yoda
    refhistos = {}
    rivet_data_dirs = [i for i in rivet.getAnalysisRefPaths() if not i.endswith("lib")]
    dirlist = []
    for d in rivet_data_dirs:
        dirlist.append([os.path.join(d, ana+'.yoda')])
    for filelist in dirlist:
        # TODO: delegate to getHistos?
        for infile in filelist:
            analysisobjects = yoda.read(infile)
            for path, ao in analysisobjects.iteritems():
                aop = rivet.AOPath(ao.path)
                if aop.isref():
                    ao.path = aop.basepath(keepref=False)
                    refhistos[ao.path] = ao
    # print refhistos
    # from IPython import embed
    # embed()
    return refhistos

def getHepDataRefData(ana):
    import rivet
    url = "http://hepdata.cedar.ac.uk/view/ins"
    url += rivet.AnalysisLoader.getAnalysis(ana).inspireId()
    url += "/yoda"
    import tempfile, urllib
    f = tempfile.NamedTemporaryFile(delete=False, suffix=".yoda")
    f.write(urllib.urlopen(url).read())
    f.close()
    import yoda
    A=yoda.read(f.name)
    return A

def gof(P1, P2):
    chi=0.0
    for num, p in enumerate(P1):
        chi += (p.y - P2[num].y)
        chi += (p.yErrAvg - P2[num].yErrAvg)
        chi += (p.x - P2[num].x)
        chi += (p.xErrAvg - P2[num].xErrAvg)
    return chi

def compareScatters(riv, hep):
    report = ""
    if len(riv.keys()) > len(hep.keys()):
        report += "\tMore Rivet histos than hepdata\n"
    if len(riv.keys()) < len(hep.keys()):
        report += "\tFewer Rivet histos than hepdata\n"
    if any(["_S" in  i for i in riv.keys()]):
        report += "\tSpires ids in Rivet histos\n"

    missing_in_hep = []
    comparable = []
    for r in riv.keys():
        ds = r.split("/")[-1]
        if not ds in [i.split("/")[-1] for i in hep.keys()]:
            report += "\t" + ds + " not in HepData\n"
            missing_in_hep.append(r)
        else:
            comparable.append([r, [i for i in hep.keys() if ds in i][0]])


    missing_in_riv = []
    for h in hep.keys():
        ds = h.split("/")[-1]
        if not ds in [i.split("/")[-1] for i in riv.keys()]:
            report += "\t" + ds + " not in Rivet\n"
            missing_in_riv.append(h)

    problems = []
    for i in comparable:
        r = riv[i[0]]
        h = hep[i[1]]


        if r.numPoints != h.numPoints:
            report += "\tNumber of points differ (%i vs %i): "%(r.numPoints, h.numPoints) + " ".join([" %s "%x for x in i]) + "\n"
            problems.append(i)
        else:
            goffit= gof(r.points, h.points)
            if abs(goffit) > 1e-8:
                report += "\t%s --- data problem of level %e\n"%(r.path, goffit)
            problems.append(i)


    return missing_in_riv, missing_in_hep, problems, report




print "Checking %i analyses"%len(all_analyses)

msg = ""
for num, a in enumerate(all_analyses):
    try:
        msg +="%s:"%a
        if not hasInspire(a):
            msg +=  "\n\tDoes not have an InspireID.\n"
        else:
            RIVET=getRivetRefData(a)
            HEPDATA=getHepDataRefData(a)

            mr, mh, cpb, rep = compareScatters(RIVET, HEPDATA)
            if len(rep)==0:
                msg+="\t --- no problems found\n"
            else:
                msg+="\n"+rep +"\n"
    except Exception, e:
        msg+= "\nError encountered when dealing with %s:"%a
        msg+= "\n\t%s\n\n"%e
    if (num+1)%5==0:
        print "Processed %i/%i"%(num+1, len(all_analyses))

fname = "report" + "".join(["_%s"%x for x in sys.argv[1:]])
fname+=".txt"
with open(fname, "w") as f:
    f.write(msg)

print "Report %s generated successfully."%fname

