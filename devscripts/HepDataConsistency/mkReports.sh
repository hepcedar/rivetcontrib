#!/bin/bash

declare -a arr=("ALEPH" "ALICE" "ARGUS" "ATLAS" "BABAR" "BELLE" "CDF" "CLEO" "CMS" "CMSTOTEM" "D0" "DELPHI" "E735" "H1" "JADE" "L3" "LHCB" "LHCF" "OPAL" "SFM" "SLD" "STAR" "TASSO" "TOTEM" "UA1" "UA5" "ZEUS")

for i in "${arr[@]}"
do
   echo "Compiling report for $i"
   /home/hschulz/src/rivet-contrib.hg/devscripts/HepDataConsistency/testData.py $i
done
