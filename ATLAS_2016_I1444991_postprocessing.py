#!/usr/bin/env python

import yoda, sys
from math import sqrt

fName = str(sys.argv[1]) if len(sys.argv) > 1 else ''

if not fName:
  sys.exit('Need to specify file name!')

hists = yoda.read( fName )
tags = sorted(hists.keys())

pre = '/ATLAS_2016_I1444991/'

f = open('%s_processed.yoda' % fName[:-5], 'w')
for h in tags:
  if '_sel' in h:  continue
  if 'd10' in h:
    y = hists[pre + 'pTj1_sel25'].bin(0).sumW
    e = sqrt(hists[pre + 'pTj1_sel25'].bin(0).sumW2)
    hists[h].point(0).y = y
    hists[h].point(0).yErrs = (e, e)

    y = hists[pre + 'd09-x01-y01'].bin(0).sumW
    e = sqrt(hists[pre + 'd09-x01-y01'].bin(0).sumW2)
    hists[h].point(1).y = y
    hists[h].point(1).yErrs = (e, e) 

    y = hists[pre + 'pTj1_sel40'].bin(0).sumW
    e = sqrt(hists[pre + 'pTj1_sel40'].bin(0).sumW2)
    hists[h].point(2).y = y
    hists[h].point(2).yErrs = (e, e)

  yoda.writeYODA(hists[h], f)
f.close()

