// -*- C++ -*-

// lhef2hepmc: a Les Houches Event Format (LHEF) -> HepMC IO_GenEvent
// MC generator event file converter
// Author: Andy Buckley, Les Houches 2011
// Hat tip: Leif Lonnblad for writing the LHEF parser that actually makes it possible!

#include "HepMC/GenEvent.h"
#include "HepMC/IO_GenEvent.h"
#include "HepMC/HepMCDefs.h"
#include "LHEF.h"
#include <cstdlib>
#include <string>
#include <cassert>
#include <iostream>
#include <numeric>

//#include <boost/preprocessor/stringize.hpp>
#define DO_APPEND0(x) x ## 0
#define APPEND0(x) DO_APPEND0(x)

using namespace std;
using namespace HepMC;

// In case IDWTUP=+/-4 one has to keep track of the accumulated weights and event numbers
// to evaluate the cross section on-the-fly. The last evaluation is the one used.
// Better to be sure that crossSection() is never used to fill the histograms, but only in the finalization stage, by reweighting the histograms with crossSection()/sumOfWeights()

double accumulated_weight = 0.0;
double accumulated_weight_squared = 0.0;
int event_number = 0;

double add_squared(const double& x, const double& y) {
  return sqrt(x*x + y*y);
}

GenParticle* initParticle(int idx, LHEF::Reader* reader) {
  FourVector p(reader->hepeup.PUP[idx][0], reader->hepeup.PUP[idx][1],
               reader->hepeup.PUP[idx][2], reader->hepeup.PUP[idx][3]);
  GenParticle* gp = new GenParticle(p, reader->hepeup.IDUP[idx], reader->hepeup.ISTUP[idx]);
  gp->set_generated_mass(reader->hepeup.PUP[idx][4]);
  return gp;
}

int main(int argc, char** argv) {

  // Look for a help argument
  for (int i = 1; i < argc; ++i) {
    const string arg = argv[i];
    if (arg == "--help" || arg == "-h") {
      cout << argv[0] << ": an LHEF to HepMC event format converter" << endl;
      cout << "Usage: " << argv[0] << " [<infile> <outfile>]" << endl;
      cout << "If called with no args, reads from stdin and writes to stdout." << endl;
      cout << "Use filename '-' to explicitly specify reading/writing on stdin/out." << endl;
      exit(0);
    }
  }

  // Choose input and output files from the command line
  string infile("-"), outfile("-");
  if (argc == 3) {
    infile = argv[1];
    outfile = argv[2];
  } else if (argc == 2 || argc > 3) {
    cerr << "Usage: " << argv[0] << " [<infile> <outfile>]" << endl;
    exit(1);
  }

  // Init readers and writers
  LHEF::Reader* reader = 0;
  if (infile == "-") {
    reader = new LHEF::Reader(cin);
  } else {
    reader = new LHEF::Reader(infile);
  }
  IO_GenEvent* writer = 0;
  if (outfile == "-") {
    writer = new IO_GenEvent(cout);
  } else {
    writer = new IO_GenEvent(outfile);
  }

  // Event loop
  while (reader->readEvent()) {
    GenEvent evt;
    evt.use_units(Units::GEV, Units::MM);

    // Set weights
    const double weight = reader->hepeup.weight(); // nominal weight
    for (size_t iw = 0; iw < reader->hepeup.weights.size(); ++iw) {
      const pair<double, const LHEF::WeightInfo*>& w = reader->hepeup.weights[iw];
      // Named weight setting requires a new HepMC
      //#warning "HEPMC_HAS_NAMED_WEIGHTS+0 = " BOOST_PP_STRINGIZE(APPEND0(HEPMC_HAS_NAMED_WEIGHTS))
      #if !defined(HEPMC_HAS_NAMED_WEIGHTS) || APPEND0(HEPMC_HAS_NAMED_WEIGHTS) < 20
      evt.weights().push_back(w.first);
      #else
      evt.weights()[w.second->name] = w.first; //< Named weights allowed
      #endif
    }

    // Event scale (may represent shower veto / hardest emission scale, variable undefined)
    const double scale = reader->hepeup.SCALUP;
    evt.set_event_scale(scale);

    // Cross-section
    #ifdef HEPMC_HAS_CROSS_SECTION
    HepMC::GenCrossSection xsec;
    const int idwtup = reader->heprup.IDWTUP;

    double xsecval = -1.0;
    double xsecerr = -1.0;
    if (abs(idwtup) == 3) {
      assert(reader->heprup.XSECUP.size() == reader->heprup.XERRUP.size());
      if(reader->heprup.XSECUP.size()) {
        xsecval = accumulate(reader->heprup.XSECUP.begin(),reader->heprup.XSECUP.end(), 0.0);
        xsecerr = accumulate(reader->heprup.XERRUP.begin(),reader->heprup.XERRUP.end(), 0.0, add_squared);
      }
      // cout << "Read cross-section = " << xsecval << " +- " << xsecerr << endl;
    } else if (abs(idwtup) == 4) {
      accumulated_weight += weight;
      accumulated_weight_squared += weight*weight;
      event_number += 1;
      xsecval = accumulated_weight/event_number;
      // xsecerr = sqrt((accumulated_weight_squared/event_number - xsecval*xsecval)/event_number);
      double xsecerr2 = (accumulated_weight_squared/event_number - xsecval*xsecval)/event_number;
      if (xsecerr2 < 0) {
        cerr << "WARNING: xsecerr^2 < 0, forcing to zero : " << xsecerr2 << endl;
        xsecerr2 = 0.0;
      }
      xsecerr = sqrt(xsecerr2);
      // cout << "Estimated cross-section = " << xsecval << " +- " << xsecerr << endl;
    } else  {
      cerr << "IDWTUP = " << idwtup << " value not handled yet. Stopping " << endl;
      exit(-1);
    }
    xsec.set_cross_section(xsecval, xsecerr);
    evt.set_cross_section(xsec);
    #endif

    std::map<int, GenParticle*> part_map;
    std::map< std::pair<int,int>, GenVertex* > vertex_map;

    // Iterate backward through the event to catch all mothers
    int idx = reader->hepeup.NUP - 1;
    while (true) {

      // If particle does not exist, initialize it
      if (part_map[idx] == 0) part_map[idx] = initParticle(idx, reader);

      // Exit loop when first particle that comes from nowhere is reached:
      // it should be an initial state particle which already was created and assigned to a vertex
      if (reader->hepeup.MOTHUP[idx].first == 0 && reader->hepeup.MOTHUP[idx].second == 0) break;

      // The LHE mother numbering starts with 1 rather than 0, so need offset for hepeup
      int mother_one = reader->hepeup.MOTHUP[idx].first - 1;
      int mother_two = reader->hepeup.MOTHUP[idx].second - 1 ;

      // If the mother index pair of the current particle does not exist, generate it and add it to the event
      if (vertex_map[std::pair<int,int>(mother_one, mother_two)] == 0) {
        GenVertex* vertex = new GenVertex();
        vertex_map[std::pair<int,int>(mother_one, mother_two)] = vertex;
        evt.add_vertex(vertex);
      }
      // Add current particle to the vertex
      vertex_map[std::pair<int,int>(mother_one, mother_two)]->add_particle_out(part_map[idx]);

      // mother == -1 here is actually mother == 0: nothing to be done
      if (mother_one != -1) {
        // If mother particle does not exist already, initialize it and add to vtx
        if (part_map[mother_one] == 0) part_map[mother_one] = initParticle(mother_one, reader);
        vertex_map[std::pair<int,int>(mother_one, mother_two)]->add_particle_in(part_map[mother_one]);
      }
      if (mother_two != -1) {
        // If mother particle does not exist already, initialize it and add to vtx
        if (part_map[mother_two] == 0) part_map[mother_two] = initParticle(mother_two, reader);
        vertex_map[std::pair<int,int>(mother_one, mother_two)]->add_particle_in(part_map[mother_two]);
      }

      // Go one index back in LHE file
      idx -= 1;
    }

    //evt.print();
    writer->write_event(&evt);
  }

  delete reader;
  delete writer;

  return EXIT_SUCCESS;
}
