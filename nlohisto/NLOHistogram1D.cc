class NLOHistogram1D : public AIDA::IHistogram1D {

  AIDA::IHistogramFactory& _factory;
  AIDA::IHistogram1D* _hist;
  LWH::Histogram1D* _tmphist;
  std::string _path;

  int _current_event_number;

  void _syncHists() {
    const IAxis & ax = _tmphist->axis();
    // the following loop is mightily inefficient, but to make it more
    // efficient one needs to modify Rivet internals
    for (int i=0; i<ax.bins(); ++i) {
      _hist->fill(ax.binLowerEdge(i)+ax.binWidth(i)/2.0,
                  _tmphist->binHeight(i));
    }
    _tmphist->reset();
  }

public:

  NLOHistogram1D(AIDA::IHistogramFactory& factory, const string& path,
                 size_t nbins, double lower, double upper) :
    _factory(factory), _path(path), _current_event_number(-1) {
    _hist = factory.createHistogram1D(_path, "", nbins, lower, upper);
    _tmphist = dynamic_cast<LWH::Histogram1D*>
      (factory.createHistogram1D(_path+"_tmp", "", nbins, lower, upper));
    if (!_tmphist) throw Error("Cast failed.");
  }
  
  NLOHistogram1D(AIDA::IHistogramFactory& factory, const string& path,
                 const vector<double>& binedges) :
    _factory(factory), _path(path), _current_event_number(-1) {
    _hist = factory.createHistogram1D(_path, "", binedges);
    _tmphist = dynamic_cast<LWH::Histogram1D*>
      (factory.createHistogram1D(_path+"_tmp", "", binedges));
    if (!_tmphist) throw Error("Cast failed.");
  }

  ~NLOHistogram1D() {}
    
  bool fill(double x, double weight = 1.) {
    std::cout<<x<<" "<<weight<<std::endl;
    throw Error("Called fill function without event.");
    return false;
  }

  bool fill(double x, const Event& event)
  {
    if (_current_event_number==-1)
      _current_event_number = event.genEvent().event_number();

    if (event.genEvent().event_number()!=_current_event_number) {
      _syncHists();
      _current_event_number = event.genEvent().event_number();
    }

    return _tmphist->fill(x, event.weight());
  }

  void finalize(Analysis* ana, const double& xs_per_event)
  {
    _syncHists();
    ana->scale(_hist, xs_per_event);
    _factory.destroy(_tmphist);
  }


  double binMean(int index) const { return _hist->binMean(index); }
  int binEntries(int index) const { return _hist->binEntries(index); }
  double binHeight(int index) const { return _hist->binHeight(index); }
  double binError(int index) const { return _hist->binError(index); }
  double mean() const { return _hist->mean(); }
  double rms() const { return _hist->rms(); }
  const IAxis & axis() const { return _hist->axis(); }
  int coordToIndex(double coord) const { return _hist->coordToIndex(coord); }
  int allEntries() const { return _hist->allEntries(); }
  int extraEntries() const { return _hist->extraEntries(); }
  double equivalentBinEntries() const { return _hist->equivalentBinEntries(); }
  double sumBinHeights() const { return _hist->sumBinHeights(); }
  double sumAllBinHeights() const { return _hist->sumAllBinHeights(); }
  double sumExtraBinHeights() const { return _hist->sumExtraBinHeights(); }
  double minBinHeight() const { return _hist->minBinHeight(); }
  double maxBinHeight() const { return _hist->maxBinHeight(); }
  int dimension() const { return _hist->dimension(); }
  bool reset() { return _hist->reset(); }
  int entries() const { return _hist->entries(); }
    
  bool add(const IHistogram1D & hist) {
    std::cout<<&hist<<std::endl;
    throw Error("Called add function in NLO histo.");
    return false;
  }
  bool scale(double scaleFactor) {
    std::cout<<scaleFactor<<std::endl;
    throw Error("Called scale function in NLO histo.");
    return false;
  }

};


NLOHistogram1D* bookNLOHistogram1D(const string& hname,
                                   size_t nbins, double lower, double upper)
{
  myMakeHistoDir();
  return new NLOHistogram1D(histogramFactory(), histoPath(hname), nbins, lower, upper);
}

NLOHistogram1D* bookNLOHistogram1D(const string& hname, const vector<double>& binedges)
{
  myMakeHistoDir();
  return new NLOHistogram1D(histogramFactory(), histoPath(hname), binedges);
}


mutable bool _myMadeHistoDir;
void myMakeHistoDir() {
  if (!_myMadeHistoDir) {
    if (! name().empty()) tree().mkdirs(histoDir());
    _myMadeHistoDir = true;
  }
}

